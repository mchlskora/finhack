import { Component, OnInit, OnDestroy } from '@angular/core';
import { FinhackService } from './finhack.service';
import { Observable } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public finhack$: Observable<string>;

  constructor(private finhackService: FinhackService, private swUpdate: SwUpdate) {}

  ngOnInit() {
    this.checkUpdate();

    this.finhack$ = this.finhackService.getRandomFinhack();
  }

  private checkUpdate(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('Dostępna jest nowa wersja. Pobrać?')) {
          window.location.reload();
        }
      });
    }
  }
}
