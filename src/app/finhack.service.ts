import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { finhacks } from './finhacks';

function getRandomInt(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min + 1)) + min;
}

@Injectable({
  providedIn: 'root',
})
export class FinhackService {
  constructor() {}

  public getRandomFinhack(): Observable<string> {
    return of(finhacks).pipe(
      map(finhacksArr => {
        const randomIndex = getRandomInt(0, finhacksArr.length - 1);

        return finhacksArr[randomIndex];
      }),
    );
  }
}
