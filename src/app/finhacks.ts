export const finhacks = [
  'Nie trwoń pieniędzy na marniejące, psujące się i pokrywające kurzem materialne rzeczy.',
  'Inwestując warto trzymać się zasady:<br />„Bój się, gdy inni są chciwi. Bądź chciwy, gdy inni się boją".',
  'Jedną z lepszych inwestycji, które zrealizowałem w swoim życiu, była... nadpłata kredytu hipotecznego.',
  'Zasada nr 1. Nie trać pieniędzy<br />Zasada nr 2. Pamiętaj o zasadzie nr 1',
  'Jesienne wyprzedaże? Kontroluj impulsy',
  'Brak planowania, to planowanie porażki',
  'Szukaj ludzi którzy dodają Ci skrzydeł!',
];
